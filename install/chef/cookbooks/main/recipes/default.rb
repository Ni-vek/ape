# execute first apt to run apt-get update
include_recipe "apt"

execute "installing or updating build essentials (make)" do
    command "cd / && apt-get -y install build-essential"
    user "root"
end
execute "installing or updating pcre" do
    command "cd / && apt-get -y install libpcre3-dev"
    user "root"
end

include_recipe "nginx"
include_recipe "php"
include_recipe "php-fpm"

execute "installing or updating php gd" do
    command "cd / && apt-get -y install php5-gd"
    user "root"
end

execute "installing or updating php curl" do
    command "cd / && apt-get -y install php5-curl"
    user "root"
end

execute "installing composer" do
    command "cd /var/www/ecole && curl -sS https://getcomposer.org/installer | php"
    user "root"
end

execute "installing or updating phalcon" do
    if File.directory?('/cphalcon')
        command "cd / && rm -R /cphalcon && git clone git://github.com/phalcon/cphalcon.git && cd cphalcon && git checkout 1.3.5 && cd build && ./install && cd / && rm /etc/php5/conf.d/phalcon.ini && # echo 'extension=phalcon.so' > /etc/php5/conf.d/phalcon.ini"
        user "root"
    else
        command "cd / && git clone git://github.com/phalcon/cphalcon.git && cd cphalcon && git checkout 1.3.5 &&  cd build && ./install && cd / && echo 'extension=phalcon.so' > /etc/php5/conf.d/phalcon.ini"
        user "root"
    end
end

include_recipe "mongodb"
php_pear "mongo" do
  action :install
end

# execute "restoring mongodb" do
#     command "cd /var/www/igtrading/install/mongodb/ && mongorestore --port #{node['mongodb']['config']['port']}"
#     user "root"
#  end

execute "restarting services" do
    command "service nginx restart && service mongodb restart && service php5-fpm restart"
    user "root"
end

execute "retrieving dependencies" do
    command "cd /var/www/ecole && php composer.phar install"
    user "root"
end

# execute "create php cli application environment" do
#    command "cd /var/www/IGTrading && export APP_ENV=\"#{node['application']['environment']}\""
#    user "root"
# end
