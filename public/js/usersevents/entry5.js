jQuery(document).ready(function(){
    var table = $('#userseventtable').DataTable();
    
    $('#userseventtable tbody').on( 'click', 'tr', function () {
		$.ajax({
                type: 'POST',
                url: '/usersevents/addAjaxEntry',
                data: {id: $(this).attr('data-id')}
			})
			.error(function (response, status, error) {
				alert(response);
			})
			.success(function (response, status, data) {
				table.row(this).remove();
				table.search( '' ).columns().search( '' ).draw();
				$('div.dataTables_filter input').focus();
            });
    });
});