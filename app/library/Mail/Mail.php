<?php

namespace Ecole\Mail;

require_once BASE_DIR . '/vendor/autoload.php';

use Phalcon\Mvc\User\Component;
use Phalcon\Mvc\View;

/**
 * Nannyster\Mail\Mail
 * Sends e-mails based on pre-defined templates
 */
class Mail extends Component {
	
	private $mail_settings;
	
	public function __construct(){
		$this->mail_settings = $this->config->mail;
	}

	/**
	 * Applies a template to be used in the e-mail
	 *
	 * @param string $name
	 * @param array $params
	 */
	public function getTemplate($name, $params) {
		$parameters = array_merge(array(
			'publicUrl' => $this->config->application->publicUrl
				), $params);

		return $this->view->getRender('emailTemplates', $name, $parameters, function ($view) {
					$view->setRenderLevel(View::LEVEL_LAYOUT);
				});
	}

	/**
	 * Sends e-mails via AmazonSES based on predefined templates
	 *
	 * @param array $to
	 * @param string $subject
	 * @param string $name
	 * @param array $params
	 */
	public function send($to, $subject, $name, $params, $tags, $replyTo = null) {

		$template = $this->getTemplate($name, $params);
		try {

			$mandrill = new \Mandrill($this->mail_settings->mandrillApiKey);
			$message = array(
				'html' => $template,
				'subject' => $subject,
				'from_email' => $this->mail_settings->fromEmail,
				'from_name' => $this->mail_settings->fromName,
				'to' => $to,
				'headers' => array('Reply-To' => ($replyTo == null ? $this->mail_settings->fromEmail : $replyTo)),
				'important' => false,
				'track_opens' => true,
				'track_clicks' => null,
				'auto_text' => null,
				'auto_html' => null,
				'inline_css' => null,
				'url_strip_qs' => null,
				'preserve_recipients' => false,
				'view_content_link' => null,
				'bcc_address' => null,
				'tracking_domain' => null,
				'signing_domain' => null,
				'return_path_domain' => null,
				'merge' => false,
				'tags' => array($tags),
				'google_analytics_domains' => null,
				'google_analytics_campaign' => null,
				'metadata' => null
			);
			$async = false;
			$ip_pool = 'Main Pool';
			$send_at = null;
			return $mandrill->messages->send($message, $async, $ip_pool, $send_at);
		}
		catch (Mandrill_Error $e) {
			// Mandrill errors are thrown as exceptions
			echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
			throw $e;
		}
	}

}
