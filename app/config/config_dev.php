<?php

return new \Phalcon\Config(array(
    'application' => array(
        'controllersDir'            => APP_DIR . '/controllers/',
        'modelsDir'                 => APP_DIR . '/models/',
        'formsDir'                  => APP_DIR . '/forms/',
        'viewsDir'                  => APP_DIR . '/views/',
        'libraryDir'                => APP_DIR . '/library/',
        'pluginsDir'                => APP_DIR . '/plugins/',
        'cacheDir'                  => APP_DIR . '/cache/',
        'logDir'                    => BASE_DIR . '/var/log/',
        'baseUri'                   => '/',
        'siteTitle'                 => '<strong>L\'école en fête - dev</strong> ',
        'pageTitle'                 => 'L\'école en fête - dev',
        'siteSlogan'                => '',
        'protocol'                  => 'http://',
        'publicUrl'                 => 'ecole',
        'domain'                    => 'ecole',
        'cryptSalt'                 => 'eEAdf+--48841f156F5rd4L!<74@[NvU]:R|_&IP_2My|jFrqd16@1ds7a9~8_!!A&+gf:+.u>/6786M754@@sqd16@1ds7a9~8_G&f4189568171@Dy6m,$D',
        'isInMaintenance'           => false
    ),
    'cookies' => array(
        'expireTime'                => 86400 * 30
    ),
    'mongo' => array(
        'username'                  => null,
        'password'                  => null,
        'host'                      => null,
        'port'                      => 32172,
        'database'                  => 'ecole'
    ),
    'mail' => array(
        'fromName'                  => 'L\'école en fête',
        'fromEmail'                 => 'contact@lecoleenfete.fr',
        'mandrillApiKey'            => '3H3mupXzBKvpbqUJMltStQ'
    )
));
