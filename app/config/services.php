<?php

use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\View;
use Phalcon\Crypt;
use Phalcon\Dispatcher;
use Phalcon\Mvc\Dispatcher as MvcDispatcher;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Flash\Session as Flash;
use Phalcon\Http\Response\Cookies;
use Phalcon\Debug;
use Phalcon\Logger\Adapter\File as FileAdapter;
use Ecole\Auth\Auth;
use Ecole\Mail\Mail;
use Ecole\Acl\Acl;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

/**
 * Register the global configuration as config
 */
$di->set('config', $config);

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
}, true);

/**
 * The debugger service
 */
$di->set('debug', function(){
    return new Debug();
});

/**
 * The logger component
 */
$di->set('logger', function() use ($config) {
    $logger = new FileAdapter($config->application->logDir . 'Ecole.app.' . date('Y.m.d') . '.log');
    chmod($config->application->logDir . 'Ecole.app.' . date('Y.m.d') . '.log', 0777);
    return $logger;
});

/**
 * Setting up the view component
 */
$di->set('view', function () use ($config) {

    $view = new View();

    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines(array(
        '.volt' => function ($view, $di) use ($config) {

            $volt = new VoltEngine($view, $di);

            $volt->setOptions(array(
                'compiledPath' => $config->application->cacheDir,
                'compiledSeparator' => '_'
            ));

            return $volt;
        },
                '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
            ));

            return $view;
        }, true);

        /**
         * Setting up collection manager for MongoDB
         */
        $di->set('collectionManager', function() {
            return new Phalcon\Mvc\Collection\Manager();
        }, true);

        /**
         * Register Mongo service
         */
        $di->set('mongo', function() use ($config) {
            $user = ($config->mongo->username !== null && $config->mongo->password !== null) ?
                    $config->mongo->username . ':' . $config->mongo->password . '@' :
                    '';
            $host = ($config->mongo->host !== null) ? $config->mongo->host : 'localhost';
            $port = ($config->mongo->port !== null) ? ':' . $config->mongo->port : ':27017';
            $database = ($config->mongo->database !== null) ? $config->mongo->database : 'test';
            $mongo = new \MongoClient('mongodb://' . $user . $host . $port);
            return $mongo->selectDB($database);
        }, true);

        /**
         * If the configuration specify the use of metadata adapter use it or use memory otherwise
         */
        /* $di->set('modelsMetadata', function () {
          return new MetaDataAdapter();
          }); */

        /**
         * Start the session the first time some component request the session service
         */
        $di->set('session', function () {
            $session = new SessionAdapter();
            $session->start();

            return $session;
        });

        /**
         * Crypt service
         */
        $di->set('crypt', function () use ($config) {
            $crypt = new Crypt();
            $crypt->setKey($config->application->cryptSalt);
            return $crypt;
        });

        /**
         * Custom authentication component
         */
        $di->set('auth', function () {
            return new Auth();
        });

        /**
         * Dispatcher use a default namespace
         */
        $di->set('dispatcher', function() use ($config, $di) {

            //Create an EventsManager
            $eventsManager = new EventsManager();
            $dispatcher = new MvcDispatcher();

            //Attach a listener
            if (APP_ENV === 'prod') {

                $eventsManager->attach("dispatch:beforeException", function($event, $dispatcher, $exception) use ($config) {
                    switch ($exception->getCode()) {
                        case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                        case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                            $dispatcher->forward(array(
                                'controller' => 'errors',
                                'action' => 'notFound'
                            ));
                            return false;
                        default :
                            $dispatcher->forward(array(
                                'controller' => 'errors',
                                'action' => 'uncaughtException'
                            ));
                            return false;
                    }
                });
            }


            //Bind the EventsManager to the dispatcher
            $dispatcher->setEventsManager($eventsManager);
            $dispatcher->setDefaultNamespace('Ecole\Controllers');
            return $dispatcher;
        }, true);

        /**
         * Loading routes from the routes.php file
         */
        $di->set('router', function () {
            return require __DIR__ . '/routes.php';
        });

        /**
         * Flash service with custom CSS classes
         */
        $di->set('flash', function () {
            return new Flash(array(
                'error' => 'dt-sc-error-box',
                'warning' => 'dt-sc-warning-box',
                'success' => 'dt-sc-info-box',
                'notice' => 'dt-sc-success-box'
            ));
        });

        /**
         * Mail service uses Mandrill
         */
        $di->set('mail', function () {
            return new Mail();
        });

        /**
         * Access Control List
         */
        $di->set('acl', function () {
            return new Acl();
        });

        /**
         * Cookies service
         */
        $di->set('cookies', function() {
            $cookies = new Cookies();
            $cookies->useEncryption(false);
            return $cookies;
        });
        