<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerNamespaces(
        array(
            'Ecole'                     => $config->application->libraryDir,
            'Ecole\Controllers'         => $config->application->controllersDir,
            'Ecole\Models'              => $config->application->modelsDir,
            'Ecole\Forms'               => $config->application->formsDir,
        )
)->register();
