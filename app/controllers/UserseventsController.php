<?php

namespace Ecole\Controllers;

use Ecole\Models\Usersevent;
use Ecole\Models\Events;
use Ecole\Forms\BookingForm;
use Ecole\Mail\Mail;

class UserseventsController extends BaseController {

	public function listAction($id) {
		$this->assets
				->collection('commonJs')
				->addJs('js/datatables.min.js')
				->addJs('js/usersevents/list.js');
		$users = Usersevent::find(array(array(
						'event_id' => new \MongoId($id)
		)));
		$nb_adults_total = 0;
		$nb_children_total = 0;
		$total_money = 0;
		foreach ($users as $user) {
			if(!$user->getIsI()){
				$nb_adults_total += $user->getNbAdults();
				$nb_children_total += $user->getNbChildren();
				$total_money += $user->getTotalInvoice();
			}
		}
		$this->view->setVar('users', $users);
		$this->view->setVar('event_id', $id);
		$this->view->setVar('nb_children_total', $nb_children_total);
		$this->view->setVar('nb_adults_total', $nb_adults_total);
		$this->view->setVar('total_money', $total_money);
	}
	
	public function entryAction($id) {
		$this->assets
				->collection('commonJs')
				->addJs('js/datatables.min.js')
				->addJs('js/usersevents/entry17.js');
		$users = Usersevent::find(array(array(
						'event_id' => new \MongoId($id)
		)));
		$nb_adults_total = 0;
		$nb_children_total = 0;
		$total_money = 0;
		foreach ($users as $user) {
			if(!$user->getIsI()){
				$nb_adults_total += $user->getNbAdults();
				$nb_children_total += $user->getNbChildren();
				$total_money += $user->getTotalInvoice();
			}
		}
		$this->view->setVar('users', $users);
		$this->view->setVar('event_id', $id);
		$this->view->setVar('nb_children_total', $nb_children_total);
		$this->view->setVar('nb_adults_total', $nb_adults_total);
		$this->view->setVar('total_money', $total_money);
	}
	
	public function addAjaxEntryAction($id){
			$user = Usersevent::findById(new \MongoId($id));
			if($user){
				$user->setIsEntered(true)
						->save();
				$this->view->disable();
				$this->flash->success('OK!');
				return $this->response->redirect('usersevents/entry/' . $user->getEventId());
			}
			else{
				$this->view->disable();
				$this->flash->error('User not found!');
				return $this->response->redirect('events/list');
			}
	}

	public function deleteAction($id) {
		$user = Usersevent::findById(new \MongoId($id));
		if ($user) {
			$user->delete();
			$this->response->redirect('usersevents/list/' . $user->getEventId());
		}
	}

	public function addAction($id) {
		$this->assets
				->collection('commonJs')
				->addJs('js/customadduserevent.js');
		$bookingForm = new BookingForm();
		$this->view->setVar('bookingForm', $bookingForm);
		$this->view->setVar('event_id', $id);
		if ($this->request->isPost()) {
			$datas = $this->request->getPost();
			$event = Events::findById(new \MongoId($id));
			if (!$bookingForm->isValid($this->request->getPost())) {
				$error = '<ul>';
				foreach ($bookingForm->getMessages() as $message) {
					$error.= '<li>' . $message . '</li>';
				}
				$error .= '</ul>';
				$this->flash->error($error);
			}
			else {
				if (!is_numeric($datas['nb_adults'])) {
					$datas['nb_adults'] = 0;
				}
				if (!is_numeric($datas['nb_children'])) {
					$datas['nb_children'] = 0;
				}
				if (isset($datas['paid'])) {
					if ($datas['paid'] == 'on') {
						$datas['paid'] = true;
					}
				}
				if (isset($datas['is_i'])) {
					if ($datas['is_i'] == 'on') {
						$datas['is_i'] = true;
					}
					else{
						$datas['is_i'] = false;
					}
				}
				$total_invoice = $datas['nb_adults'] * $event->getAdultsPrice() + $datas['nb_children'] * $event->getChildrenPrice();
				if ($total_invoice > 0) {
					$total_users = Usersevent::count(array(array('paid' => true, 'event_id' => $event->getId())));
					$reservation_id = $total_users + 1;
					$userevent = new Usersevent();
					if ($datas['paid']) {
						$userevent->setReservationId($reservation_id);
					}
					if($datas['is_i']){
						$userevent->setIsI($datas['is_i']);
					}
					$userevent->setEmail($datas['email'])
							->setName($datas['name'])
							->setFirstname($datas['firstname'])
							->setNbAdults($datas['nb_adults'])
							->setNbChildren($datas['nb_children'])
							->setEventId($event->getId())
							->setPaid($datas['paid'])
							->setTotalInvoice($total_invoice)
							->save();

					if ($datas['paid'] && APP_ENV != 'dev') {
						$mail = new Mail();
						$recipients = array(
							array(
								'email' => $datas['email'],
								'name' => $datas['firstname'] . ' ' . $datas['name'],
								'type' => 'to'
							)
						);
						$result = $mail->send($recipients, 'Votre numéro de réservation: ' . $event->getTitle(), 'registrationConfirmation', array('event_title' => $event->getTitle(), 'event_date' => $event->getEventDate()->sec, 'email' => $datas['email'], 'nb_children' => $datas['nb_children'], 'nb_adults' => $datas['nb_adults'], 'total_invoice' => $total_invoice, 'reservation_id' => $reservation_id), 'Booking');
					}

					$this->flash->success('User added!');
				}
				else {
					$this->flash->error('Vous ne pouvez pas réserver pour 0 personnes!!!');
				}
			}
			if(APP_ENV != 'dev'){
				return $this->response->redirect('usersevents/add/' . $event->getId());
			}
			else{
				return $this->response->redirect('usersevents/entry/' . $event->getId());
			}
		}
	}
	
	public function recallAction($id){
		$userevent = Usersevent::findById($id);
		if($userevent){
			$event = Events::findById($userevent->getEventId());
			
			$userevent->setRecalled(new \MongoDate())->save();
			$mail = new Mail();
			$recipients = array(
				array(
					'email' => $userevent->getEmail(),
					'name' => $userevent->getFirstname() . ' ' . $userevent->getName(),
					'type' => 'to'
				)
			);
			$result = $mail->send($recipients, 'Rappel de réservation: ' . $event->getTitle(), 'recallRegistration', array('event_title' => $event->getTitle(), 'event_date' => $event->getEventDate()->sec, 'email' => $userevent->getEmail(), 'nb_children' => $userevent->getNbChildren(), 'nb_adults' => $userevent->getNbAdults(), 'total_invoice' => $userevent->getTotalInvoice()), 'Booking');
		}
		return $this->response->redirect('usersevents/list/' . $event->getId());
	}

	public function editAction($id) {
		$user = Usersevent::findById(new \MongoId($id));
		$event = Events::findById(new \MongoId($user->getEventId()));
		$this->view->setVar('user', $user);
		if ($this->request->isPost()) {
			$datas = $this->request->getPost();
			$total_invoice = $datas['nb_adults'] * $event->getAdultsPrice() + $datas['nb_children'] * $event->getChildrenPrice();
			$user->setEmail($datas['email'])
					->setName($datas['name'])
					->setFirstname($datas['firstname'])
					->setNbAdults($datas['nb_adults'])
					->setNbChildren($datas['nb_children'])
					->setPaid($datas['paid'])
					->setTotalInvoice($total_invoice)
					->save();
		}
	}

	public function validatepaiementAction($id) {
		$userevent = Usersevent::findById(new \MongoId($id));
		if ($userevent) {
			$total_users = Usersevent::count(array(array('paid' => true, 'event_id' => $userevent->getEventId())));
			$event = Events::findById($userevent->getEventId());
			$reservation_id = $total_users + 1;
			if ($userevent->getReservationId() == null) {
				$userevent->setReservationId($reservation_id)
						->setPaid(true)
						->save();
			}
			else{
				$userevent->setPaid(true)
						->save();
			}

			$mail = new Mail();
			$recipients = array(
				array(
					'email' => $userevent->getEmail(),
					'name' => $userevent->getFirstname() . ' ' . $userevent->getName(),
					'type' => 'to'
				)
			);
			$result = $mail->send($recipients, 'Votre numéro de réservation: ' . $event->getTitle(), 'registrationConfirmation', array('event_title' => $event->getTitle(), 'event_date' => $event->getEventDate()->sec, 'email' => $userevent->getEmail(), 'nb_children' => $userevent->getNbChildren(), 'nb_adults' => $userevent->getNbAdults(), 'total_invoice' => $userevent->getTotalInvoice(), 'reservation_id' => $userevent->getReservationId()), 'Booking');

			$this->flash->success('User paiement validated!');
			$this->response->redirect('usersevents/list/' . $userevent->getEventId());
		}
	}

}
