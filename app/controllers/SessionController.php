<?php

namespace Ecole\Controllers;

use Phalcon\Mvc\View;
use Ecole\Forms\SigninForm;
use Ecole\Models\Users;
use Ecole\Models\Profiles;

class SessionController extends BaseController
{

    /**
     * Login a user
     */
    public function loginAction()
    {
        $this->tag->prependTitle('Connexion - ');
        $this->view->setVar('activeClass', 'login');
        $this->view->setVar('breadcrumbs', array('Connexion' => array('last' => true)));
        $loginForm = new SigninForm();
        if ($this->request->isPost()) {
            try {
                //Request is not valid
                if (!$loginForm->isValid($this->request->getPost())) {
                    $error = '<ul>';
                    foreach ($loginForm->getMessages() as $message) {
                        $error.= '<li>' . $message . '</li>';
                    }
                    $error .= '</ul>';
                    $this->flash->error($error);
                }
                // Request is valid
                else {
                    $this->auth->check(array(
                        'email' => $this->request->getPost('email'),
                        'password' => $this->request->getPost('password'),
                        'remember' => $this->request->getPost('remember')
                    ));
                    //Redirection to the change password page if needed
                    if ($this->auth->getIdentity()->getMustChangePassword()) {
                        return $this->response->redirect('users/changePassword');
                    }
                    //Classical redirection after login
                    elseif(strpos($this->request->getHTTPReferer(), '/session/') !== false){
                        return $this->response->redirect('index');
                    }
                    //Redirection after login on a specific page that have been called before login
                    else{
                        return $this->response->redirect($this->request->getHTTPReferer());
                    }
                }
            }
            catch (\Exception $e) {
                $this->flash->error($e->getMessage());
            }
        }
        $this->view->loginForm = $loginForm;
    }


    /**
     * Closes the session
     */
    public function logoutAction()
    {
        $this->auth->remove();
        return $this->response->redirect('index');
    }

}