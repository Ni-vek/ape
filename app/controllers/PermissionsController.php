<?php
namespace Ecole\Controllers;

use Ecole\Models\Profiles;
use Ecole\Models\Permissions;

/**
 * View and define permissions for the various profile levels.
 */
class PermissionsController extends BaseController
{

    /**
     * View the permissions for a profile level, and change them if we have a POST.
     */
    public function indexAction()
    {
        $this->tag->prependTitle('Permissions - ');
        $this->view->setVar('activeClass', 'administration');
        $this->view->setVar('breadcrumbs', array(
            'Administration' => array(
                'controller' => 'administration',
                'action' => 'index'),
            'Permissions' => array(
                'last' => true)
        ));

        if ($this->request->isPost()) {
                if ($this->request->hasPost('permissions')) {

                    // Deletes the current permissions
                    $permissions = Permissions::find();
                    foreach ($permissions as $permission) {
                        $permission->delete();
                    }

                    // Save the new permissions
                    foreach ($this->request->getPost('permissions') as $profile_id => $permissions) {
                        foreach ($permissions as $permission => $on) {
                            
                            $parts = explode('.', $permission);

                            $acl_permission = new Permissions();
                            $acl_permission->setProfileId(new \MongoId($profile_id))
                                    ->setResource($parts[0])
                                    ->setAction($parts[1])
                                    ->save();
                        }
                    }

                    $this->flash->success('Les permissions ont bien été mises à jour!');
                }

                // Rebuild the ACL with
                $this->acl->rebuild();
        }

        // Pass all the active profiles
        $this->view->profiles = Profiles::find(array(array(
            'active' => true)));
        $this->view->permissions = $this->acl->getAllPermissions();
    }
}