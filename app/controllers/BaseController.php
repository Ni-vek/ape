<?php

namespace Ecole\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\View;
use Ecole\Models\Accounts;

class BaseController extends Controller {

	public function beforeDispatch(Dispatcher $dispatcher) {
		
	}

	public function beforeExecuteRoute(Dispatcher $dispatcher) {
		$controllerName = $dispatcher->getControllerName();
		$actionName = $dispatcher->getActionName();
		
		/*if ($actionName == 'index' && $controllerName == 'index') {
			return $this->response->redirect('events/view/567a6debf0cc05ed478b4567');
		}*/
		//Check if user has remember me!
		if (!$this->acl->isAllowed($this->auth->hasIdentity() ? $this->auth->getProfile()->getName() : 'Guest', $controllerName, $actionName)) {
			$this->flash->error('Vous n\'êtes pas autorisé à accéder à cette zone!');
			$this->view->disable();
			return $this->response->redirect('index');
		}
	}

	public function initialize() {
		//Cheats to test if pjax is down or not
		/* if (APP_ENV !== 'prod') {
		  sleep(1);
		  } */
		// Define template
		$this->view->setTemplateBefore('landing');


		//Retrieve each accounts
//        $this->view->setVar('accounts', Accounts::find());
		//Last part of the title
		$this->tag->setTitle($this->config->application->pageTitle . ' - ' . $this->config->application->siteSlogan);

		//App collection of CSS files
		$this->assets
				->collection('appCss')
				->addCss('css/style.css')
				->addCss('css/shortcodes.css')
				->addCss('css/responsive.css')
				->addCss('css/font-awesome.min.css')
				->addCss('css/layerslider.css')
				->addCss('css/prettyPhoto.css')
				->addCss('js/datatables.min.css')
				->addCss('css/fonts.googleapis.com/cssea9f.css?family=Lato:300,400,700,300italic,400italic,700italic')
				->addCss('css/fonts.googleapis.com/css5c84.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800')
				->addCss('css/fonts.googleapis.com/css76b9.css?family=Bubblegum+Sans');

		//Head Js
		$this->assets
				->collection('headJs')
				->addJs('js/modernizr-2.6.2.min.js');


		//Common scripts
		$this->assets
				->collection('commonJs')
				->addJs('js/jquery.js')
				->addJs('js/jquery-migrate.min.js')
				->addJs('http://maps.google.com/maps/api/js?sensor=false')
				->addJs('js/jquery.gmap.min.js')
				->addJs('js/jquery.validate.min.js')
				->addJs('js/jquery-easing-1.3.js')
				->addJs('js/jquery.sticky.js')
				->addJs('js/jquery.nicescroll.min.js')
				->addJs('js/jquery.inview.js')
				->addJs('js/validation.js')
				->addJs('js/jquery.tipTip.minified.js')
				->addJs('js/jquery.bxslider.min.js')
				->addJs('js/jquery.prettyPhoto.js')
				->addJs('js/twitter/jquery.tweet.min.js')
				->addJs('js/jquery.parallax-1.1.3.js')
				->addJs('js/shortcodes.js')
				->addJs('js/jquery.carouFredSel-6.2.0-packed.js')
				->addJs('js/custom.js');

		//App scripts
		$this->assets
				->collection('layerSliderJs')
				->addJs('js/jquery-transit-modified.js')
				->addJs('js/layerslider.kreaturamedia.jquery.js')
				->addJs('js/greensock.js')
				->addJs('js/layerslider.transitions.js');

		// Pass the identity to each view called
		$this->view->setVar('identity', $this->auth->getIdentity());
		$this->view->setVar('profile', $this->auth->hasIdentity() ? $this->auth->getProfile()->getName() : 'Guest');
	}

	public function validateMongoId($id) {
		if (preg_match('/^[0-9a-z]{24}$/', $id)) {
			return true;
		}
		else {
			return false;
		}
	}

	public static function uuidV4() {
		return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
				// 32 bits for "time_low"
				mt_rand(0, 0xffff), mt_rand(0, 0xffff),
				// 16 bits for "time_mid"
				mt_rand(0, 0xffff),
				// 16 bits for "time_hi_and_version",
				// four most significant bits holds version number 4
				mt_rand(0, 0x0fff) | 0x4000,
				// 16 bits, 8 bits for "clk_seq_hi_res",
				// 8 bits for "clk_seq_low",
				// two most significant bits holds zero and one for variant DCE1.1
				mt_rand(0, 0x3fff) | 0x8000,
				// 48 bits for "node"
				mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
		);
	}

}
