<?php

namespace Ecole\Controllers;

use Ecole\Models\Events;

class IndexController extends BaseController {

	public function indexAction() {
		$this->view->setVar('activeClass', 'home');
		$events = Events::find(array(
					'sort' => array(
						'event_date' => -1
					),
					array(
						'active' => true
					)
		));
		$this->view->setVar('events', $events);
	}

}
