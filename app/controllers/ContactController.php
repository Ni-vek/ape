<?php

namespace Ecole\Controllers;

use Ecole\Forms\ContactForm;
use Ecole\Mail\Mail;

class ContactController extends BaseController
{
	public function indexAction(){
		
		$this->view->setVar('activeClass', 'contact');
		$contactForm = new ContactForm();
		$this->view->contactForm = $contactForm;
		if ($this->request->isPost()) {
			$datas = $this->request->getPost();
			if (!$contactForm->isValid($this->request->getPost())) {
				$error = '<ul>';
				foreach ($contactForm->getMessages() as $message) {
					$error.= '<li>' . $message . '</li>';
				}
				$error .= '</ul>';
				$this->flash->error($error);
			}
			else {
				$mail = new Mail();
					$recipients = array(
						array(
							'email' => 'contact@lecoleenfete.fr',
							'name' => 'L\'école en fête',
							'type' => 'to'
						)
					);
				$result = $mail->send($recipients, 'Message de ' . $datas['name'] . ' : ' . $datas['subject'], 'contact', array('email' => $datas['email'], 'name' => $datas['name'], 'message' => $datas['message']), 'Contact', $datas['email']);
				$this->flash->success('Votre message a bien été envoyé.');
			}
			return $this->response->redirect('contact');
		}
	}
}