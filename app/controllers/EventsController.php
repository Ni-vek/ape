<?php

namespace Ecole\Controllers;

use Ecole\Models\Billets;
use Ecole\Models\Events;
use Ecole\Models\Usersevent;
use Ecole\Utils\Images;
use Ecole\Mail\Mail;
use Ecole\Forms\BookingForm;

class EventsController extends BaseController {

	public function indexAction() {
		$this->view->setVar('activeClass', 'events');
		$billets = Billets::find(array(
					'sort' => array(
						'date_creation' => -1
					),
					array(
						'active' => true
					)
		));
		$this->view->setVar('billets', $billets);
	}
	
	public function recallAction($id){
		$event = Events::findById(new \MongoId($id));
		$users = Usersevent::find(array(array(
			'event_id' => $event->getId()
		)));
		try {
			$recipients = array();
			foreach ($users as $user) {
				array_push($recipients,
					array(
						'email' => $user->getEmail(),
						'name' => $user->getFirstname() . ' ' . $user->getName(),
						'type' => 'to'
					)
				);
			}
			$mail = new Mail();
			$result = $mail->send($recipients, $event->getTitle() . ' : C\'est demain!!!', 'recall', array(
				'event_title' => $event->getTitle(), 
				'event_date' => $event->getEventDate()->sec, 
				'email' => 0, 
				'nb_children' => 0, 
				'nb_adults' => 0, 
				'total_invoice' => 0, 
				'reservation_id' => 0
					), 'Recall');
		} catch (Exception $ex) {
			$this->flash->error('Recall error!');
			$this->view->disable();
			return $this->response->redirect('events/list');
		}
		$this->flash->success('Recall sent!');
		$this->view->disable();
		return $this->response->redirect('events/list');
	}

	public function viewAction($id) {
		$this->view->setVar('activeClass', 'events');
		if (self::validateMongoId($id)) {
			$event = Events::findById(new \MongoId($id));
			if ($event) {
				$this->view->setVar('event', $event);
			}
			else {
				return $this->response->redirect('index');
			}
		}
		else {
			return $this->response->redirect('index');
		}
		$this->view->bookingForm = new BookingForm();
	}

	public function createAction() {
		$this->view->setVar('activeClass', 'admin');
		if ($this->request->isPost()) {
			$datas = $this->request->getPost();
			$event = new Events();
			if ($datas['active_registrations'] == 'on') {
				$datas['active_registrations'] = true;
			}
			else {
				$datas['active_registrations'] = false;
			}
			if ($datas['active'] == 'on') {
				$datas['active'] = true;
			}
			else {
				$datas['active'] = false;
			}
			$datas['event_date'] = new \MongoDate(strtotime($datas['event_date']));
			$datas['booking_start_date'] = new \MongoDate(strtotime($datas['booking_start_date']));
		}
		if ($this->request->hasFiles()) {


			//define upload folder
			$uploadFolder = "/public/images";

			//define full path to upload folder
			$uploadPath = BASE_DIR . $uploadFolder;
			// Proceed each images
			foreach ($this->request->getUploadedFiles() as $file) {

				//Check file type
				switch ($file->getType()) {
					case 'image/png':
						$ext = 'png';
						break;

					case 'image/jpeg':
						$ext = 'jpeg';
						break;

					case 'image/gif':
						$ext = 'gif';
						break;

					default:
						die('{"error":true, "message": "Votre photo n\'est pas au bon format. Vous ne pouvez télécharger que des photos aux formats .jpeg, .png ou .gif. Veuillez recommencer", "oldImage": "' . $user['avatar'] . '"}');
						break;
				}

				if ($file->getError() == 0) {

					//Define new image name
					$newImageName = self::uuidV4() . '.' . $ext;

					//check if file exists in upload folder
					while (file_exists($uploadPath . '/' . $newImageName)) {
						//Define a newer image name
						$newImageName = self::uuidV4() . '.' . $ext;
					}

					//create full path with image name
					$full_image_path = $uploadPath . '/' . $newImageName;

					//Move the picture to img/avatar folder
					try {
						$img = new Images($file);
						$img->best_fit(800, 800)->save($full_image_path);

						//Save it in mongo
						$event->setImage($newImageName);
					}
					catch (Exception $e) {
						die('{"error":true, "message": "Il y a eu une erreur lors du téléchargement de votre photo. Veuillez recommencer", "oldImage": "' . $user->getAvatarFile() . '"}');
					}
				}
				else {
					die('{"error":true, "message": "Il y a eu une erreur lors du téléchargement de votre photo. Veuillez recommencer", "oldImage": "' . $user->getAvatarFile() . '"}');
				}
			}
			unset($datas['image']);
		}
		if ($this->request->isPost()) {
			unset($datas['submit']);
			$event->hydrate($datas);
			$event->save();
			$this->flash->success('Event created');
			return $this->response->redirect('events/view/' . $event->getId());
		}
	}

	public function bookingAction() {
		$bookingForm = new BookingForm();
		if ($this->request->isPost()) {
			$datas = $this->request->getPost();
			$event = Events::findById(new \MongoId($datas['event_id']));
			if (!$bookingForm->isValid($this->request->getPost())) {
				$error = '<ul>';
				foreach ($bookingForm->getMessages() as $message) {
					$error.= '<li>' . $message . '</li>';
				}
				$error .= '</ul>';
				$this->flash->error($error);
			}
			else {
				if (!is_numeric($datas['nb_adults'])) {
					$datas['nb_adults'] = 0;
				}
				if (!is_numeric($datas['nb_children'])) {
					$datas['nb_children'] = 0;
				}
				$total_invoice = $datas['nb_adults'] * $event->getAdultsPrice() + $datas['nb_children'] * $event->getChildrenPrice();
				if ($total_invoice > 0) {
					$userevent = new Usersevent();
					$userevent->setEmail($datas['email'])
							->setName($datas['name'])
							->setFirstname($datas['firstname'])
							->setNbAdults($datas['nb_adults'])
							->setNbChildren($datas['nb_children'])
							->setEventId($event->getId())
							->setPaid(false)
							->setTotalInvoice($total_invoice)
							->save();


					$mail = new Mail();
					$recipients = array(
						array(
							'email' => $datas['email'],
							'name' => $datas['firstname'] . ' ' . $datas['name'],
							'type' => 'to'
						)
					);
					$result = $mail->send($recipients, 'Réservation: ' . $event->getTitle(), 'newRegistration', array('email' => $datas['email'], 'nb_children' => $datas['nb_children'], 'nb_adults' => $datas['nb_adults'], 'total_invoice' => $total_invoice), 'Booking');
					$this->flash->success('L\'école en fête vous remercie. Un email vient de vous être envoyé.');
				}
				else {
					$this->flash->error('Vous ne pouvez pas réserver pour 0 personnes!!!');
				}
			}
			return $this->response->redirect('events/view/' . $event->getId());
		}
		else {
			$this->flash->error('Bad request!!!');
			return $this->response->redirect('index');
		}
	}

	public function listAction() {
		$this->view->setVar('activeClass', 'admin');
		$events = Events::find();
		$this->view->setVar('events', $events);
	}

	public function deleteAction($id) {
		$event = Events::findById(new \MongoId($id));
		if ($event) {
			$event->delete();
			$this->flash->success('Event deleted');
		}
		else {
			$this->flash->error('Event not found');
		}
		return $this->response->redirect('events/list');
	}
	
	public function editAction($id){
		$event = Events::findById(new \MongoId($id));
		if($this->request->isPost()){
			$datas = $this->request->getPost();
			if ($datas['active_registrations'] == 'on') {
				$datas['active_registrations'] = true;
			}
			else {
				$datas['active_registrations'] = false;
			}
			if ($datas['active'] == 'on') {
				$datas['active'] = true;
			}
			else {
				$datas['active'] = false;
			}
			$datas['event_date'] = new \MongoDate(strtotime($datas['event_date']));
			$datas['booking_start_date'] = new \MongoDate(strtotime($datas['booking_start_date']));
			unset($datas['submit']);
			$event->hydrate($datas);
			$event->save();
			$this->flash->success('Event updated');
			return $this->response->redirect('events/view/' . $event->getId());
		}
		else{
			if($event){
				$this->view->setVar('event', $event);
			}
			else{
				$this->flash-error('Event not found');
				return $this->response->redirect('events/list');
			}
		}
	}

}
