<?php

namespace Ecole\Controllers;
use Ecole\Models\Billets;


class AdminController extends BaseController
{

    public function indexAction()
    {
        $this->view->setVar('activeClass', 'admin');
    }
	
	public function createuserAction(){
		$user = new \Ecole\Models\Users();
		$profile = \Ecole\Models\Profiles::find(array(array('name' => 'Administrateur')));
		$user->setActive(true)
				->setEmail('nadia.renaudineau@hotmail.fr')
				->setFirstname('Nadia')
				->setMobile('0684483129')
				->setPassword('nadiarenaudineau')
				->setProfile($profile)
				->save();
	}

}
