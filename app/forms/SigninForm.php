<?php
namespace Ecole\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email as EmailValidator;

class SigninForm extends Form
{

    public function initialize()
    {
        // Email
        $email = new Email('email', array(
            'placeholder' => 'Email'
        ));
        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'L\'email est obligatoire!'
            )),
            new EmailValidator(array(
                'message' => 'Votre email n\'est pas valide!'
            ))
        ));
        $this->add($email);

        // Password
        $password = new Password('password', array(
            'placeholder' => 'Mot de passe'
        ));
        $password->addValidator(new PresenceOf(array(
            'message' => 'Votre mot de passe est obligatoire!'
        )));
        $this->add($password);
    }
}
