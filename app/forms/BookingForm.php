<?php
namespace Ecole\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Numeric;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email as EmailValidator;

class BookingForm extends Form
{

    public function initialize()
    {
        // Email
        $email = new Email('email', array(
            'placeholder' => 'Email'
        ));
        if(APP_ENV != 'dev'){
			$email->addValidators(array(
				new PresenceOf(array(
					'message' => 'L\'email est obligatoire!'
				)),
				new EmailValidator(array(
					'message' => 'Votre email n\'est pas valide!'
				))
			));
		}
        $this->add($email);
		
		$name = new Text('name', array(
			'placeholder' => 'Nom'
		));
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'Votre nom est obligatoire!'
            ))
        ));
        $this->add($name);
		
		$firstname = new Text('firstname', array(
			'placeholder' => 'Prénom'
		));
        $firstname->addValidators(array(
            new PresenceOf(array(
                'message' => 'Votre prénom est obligatoire!'
            ))
        ));
        $this->add($firstname);
		
		$nb_adults = new Numeric('nb_adults', array(
			'placeholder' => 'Nb d\'adultes'
		));
        $this->add($nb_adults);
		
		$nb_children = new Numeric('nb_children', array(
			'placeholder' => 'Nb d\'enfants'
		));
        $this->add($nb_children);
    }
}
