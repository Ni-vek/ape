<?php
namespace Ecole\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Numeric;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email as EmailValidator;

class ContactForm extends Form
{

    public function initialize()
    {
        // Email
        $email = new Email('email', array(
            'placeholder' => 'Email'
        ));
        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'L\'email est obligatoire!'
            )),
            new EmailValidator(array(
                'message' => 'Votre email n\'est pas valide!'
            ))
        ));
        $this->add($email);
		
		$name = new Text('name', array(
			'placeholder' => 'Nom'
		));
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'Votre nom est obligatoire!'
            ))
        ));
        $this->add($name);
		
		$subject = new Text('subject', array(
			'placeholder' => 'Sujet'
		));
        $this->add($subject);
		
		$message = new Textarea('message', array(
			'placeholder' => 'Message'
		));
        $message->addValidators(array(
            new PresenceOf(array(
                'message' => 'Votre message ne peut pas être vide!'
            ))
        ));
        $this->add($message);
    }
}
