<?php

namespace Ecole\Models;

class Profiles extends BaseModel
{

    /**
     * The profile object ID
     * 
     * @var \MongoID
     */
    public $_id;

    /**
     * Define if a profile is active are not
     * 
     * @var boolean
     */
    public $active;

    /**
     * The name of the profile
     * 
     * @var string
     */
    public $name;

    /**
     * Define if a profile is visible are not.
     * For example, in the register form, the administrator profile must not be visible
     * 
     * @var boolean
     */
    public $visible;

    public function getId()
    {
        return $this->_id;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getStyle()
    {
        switch ($this->getName()) {
            case 'Super Administrateur':
                return 'danger';
            case 'Administrateur':
                return 'warning';
            case 'Membre':
                return 'info';
        }
    }

    public function getVisible()
    {
        return $this->visible;
    }

    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setVisible($visible)
    {
        $this->visible = $visible;
        return $this;
    }

}