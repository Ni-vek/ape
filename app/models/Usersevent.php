<?php

namespace Ecole\Models;

class Usersevent extends BaseModel {

	/**
     * The user event object ID
     * 
     * @var \MongoID
     */
	public $_id;
	
	/**
	 *
	 * @var string
	 */
	public $email;
	
	/**
	 *
	 * @var string
	 */
	public $name;
	
	/**
	 *
	 * @var string
	 */
	public $firstname;
	
	/**
	 *
	 * @var integer
	 */
	public $nb_children;
	
	/**
	 *
	 * @var integer
	 */
	public $nb_adults;
	
	/**
	 *
	 * @var \MongoID
	 */
	public $event_id;
	
	/**
	 *
	 * @var double
	 */
	public $total_invoice;
	
	/**
	 *
	 * @var boolean
	 */
	public $paid = false;
	
	/**
	 *
	 * @var boolean
	 */
	public $is_i = false;
	
	/**
	 *
	 * @var boolean
	 */
	public $is_entrered = false;
	
	/**
	 *
	 * @var \MongoDate
	 */
	public $created;
	
	/**
	 *
	 * @var \MongoDate
	 */
	public $recalled;
	
	/**
	 *
	 * @var \MongoDate
	 */
	public $updated;
	
	/**
	 *
	 * @var \MongoDate
	 */
	public $paiement_date;
	
	/**
	 *
	 * @var integer
	 */
	public $reservation_id = null;
	
	public function getId() {
		return $this->_id;
	}

	public function getEmail() {
		return $this->email;
	}

	public function getName() {
		return $this->name;
	}

	public function getFirstname() {
		return $this->firstname;
	}

	public function getNbChildren() {
		return $this->nb_children;
	}

	public function getNbAdults() {
		return $this->nb_adults;
	}

	public function getEventId() {
		return $this->event_id;
	}

	public function getTotalInvoice() {
		return $this->total_invoice;
	}

	public function getPaid() {
		return $this->paid;
	}

	public function getIsI() {
		return $this->is_i;
	}

	public function getIsEntered() {
		return $this->is_entrered;
	}

	public function getCreated() {
		return $this->created;
	}
	
	public function getRecalled(){
		return $this->recalled;
	}

	public function getUpdated() {
		return $this->updated;
	}

	public function getPaiementDate() {
		return $this->paiement_date;
	}

	public function getReservationId() {
		return $this->reservation_id;
	}

	public function setEmail($email) {
		$this->email = $email;
		return $this;
	}

	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	public function setFirstname($firstname) {
		$this->firstname = $firstname;
		return $this;
	}

	public function setNbChildren($nb_children) {
		$this->nb_children = $nb_children;
		return $this;
	}

	public function setNbAdults($nb_adults) {
		$this->nb_adults = $nb_adults;
		return $this;
	}

	public function setEventId(\MongoID $event_id) {
		$this->event_id = $event_id;
		return $this;
	}

	public function setTotalInvoice($total_invoice) {
		$this->total_invoice = $total_invoice;
		return $this;
	}

	public function setPaid($paid) {
		$this->paid = $paid;
		return $this;
	}

	public function setIsI($is_i) {
		$this->is_i = $is_i;
		return $this;
	}

	public function setIsEntered($is_entrered) {
		$this->is_entrered = $is_entrered;
		return $this;
	}

	public function setCreated(\MongoDate $created) {
		$this->created = $created;
		return $this;
	}
	
	public function setRecalled(\MongoDate $recalled){
		$this->recalled = $recalled;
		return $this;
	}

	public function setUpdated(\MongoDate $updated) {
		$this->updated = $updated;
		return $this;
	}

	public function setPaiementDate(\MongoDate $paiement_date) {
		$this->paiement_date = $paiement_date;
		return $this;
	}

	public function setReservationId($reservation_id) {
		$this->reservation_id = $reservation_id;
		return $this;
	}

	public function afterValidationOnCreate(){
		$this->setCreated(new \MongoDate());
	}

}