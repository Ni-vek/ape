<?php

namespace Ecole\Models;

class Users extends BaseModel
{

    /**
     * The user object ID
     * 
     * @var \MongoID
     */
    public $_id;

    /**
     * Define if the user is active
     * 
     * @var boolean
     */
    public $active = false;

    /**
     * User's avatar file
     * 
     * @var string
     */
    public $avatar_file = 'cartoon11.jpg';

    /**
     * Define if the user is banned
     * 
     * @var boolean
     */
    public $banned = false;

    /**
     * The creation date
     * 
     * @var \MongoDate
     */
    public $created;

    /**
     * User's email
     * 
     * @var string
     */
    public $email;

    /**
     * User's firstname
     * 
     * @var string
     */
    public $firstname;

    /**
     * User's mobile number
     * 
     * @var string
     */
    public $mobile;
    
    /**
     * Defineif the user must change tis password
     *
     * @var boolean
     */
    public $must_change_password = false;

    /**
     * User's password
     * 
     * @var string
     */
    public $password;

    /**
     * User's profile
     * 
     * @var \MongoID 
     */
    public $profile;

    /**
     * User's profile
     * 
     * @var Nannyster\Models\Profiles
     */
    private $profile_object = null;

    /**
     * User's update date
     * 
     * @var \MongoDate
     */
    public $updated;
    
    public function onConstruct($datas = null)
    {

        if ($datas) {
            $this->hydrate($datas);
        }
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function getAvatarFile()
    {
        return $this->avatar_file;
    }

    public function getBanned()
    {
        return $this->banned;
    }
    
    public function getCreated()
    {
        return $this->created;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getFirstname()
    {
        return $this->firstname;
    }

    public function getMobile()
    {
        return $this->mobile;
    }

    public function getMustChangePassword()
    {
        return $this->must_change_password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getProfile()
    {
        if ($this->profile_object === null && $this->profile !== null) {
            $this->setProfile(Profiles::findById($this->profile));
        }
        return $this->profile_object;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    public function setAvatarFile($avatar_file)
    {
        $this->avatar_file = $avatar_file;
        return $this;
    }

    public function setBanned($banned)
    {
        $this->banned = $banned;
        return $this;
    }

    public function setCreated(\MongoDate $created)
    {
        $this->created = $created;
        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
        return $this;
    }

    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
        return $this;
    }

    public function setMustChangePassword($must_change_password)
    {
        $this->must_change_password = $must_change_password;
        return $this;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function setProfile(\Ecole\Models\Profiles $profile)
    {
        $this->profile_object = $profile;
        $this->profile = $profile->getId();
        return $this;
    }

    public function setSuspended($suspended)
    {
        $this->suspended = $suspended;
        return $this;
    }

    public function setUpdated(\MongoDate $updated)
    {
        $this->updated = $updated;
        return $this;
    }

    public function afterValidationOnCreate()
    {
        $this->setCreated(new \MongoDate());
    }

    public function afterValidationOnUpdate()
    {
        $this->setUpdated(new \MongoDate());
    }

    /**
     * Send a e-mail confirmation to the user if the account is not active
     */
    /*public function afterCreate()
    {
        if (!$this->getActive()) {
            $emailConfirmation = new EmailConfirmations();
            $emailConfirmation->setUser($this);
            if ($emailConfirmation->save() && !$this->getCreatedByNanny()) {
                $this->getDI()
                        ->getFlash()
                        ->notice('Un email de confirmation vient de vous être envoyé à l\'adresse ' . $this->getEmail() . ', pour activer votre compte, veuillez suivre les indications contenues dans l\'email');
            }
        }
    }*/

    public function fetchForSession()
    {
        $this->getProfile();
        return $this;
    }

    public function randomPassword()
    {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

}