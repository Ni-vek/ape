<?php

namespace Ecole\Models;

class Events extends BaseModel {

	/**
     * The Billet object ID
     * 
     * @var \MongoID
     */
	public $_id;
	
	/**
     *
     * 
     * @var string
     */
	public $title;
	
	/**
     *
     * 
     * @var string
     */
	public $content;
	
	/**
     *
     * 
     * @var string
     */
	public $special_content;
	
	/**
     *
     * 
     * @var \MongoDate
     */
	public $event_date;
	
	/**
     *
     * 
     * @var array
     */
	public $image;
	
	/**
     *
     * 
     * @var array
     */
	public $sponsors;
	
	/**
     *
     * 
     * @var string
     */
	public $resume;
	
	/**
     *
     * 
     * @var boolean
     */
	public $active_registrations = false;
	
	/**
     *
     * 
     * @var \MongoDate
     */
	public $booking_start_date;
	
	/**
     *
     * 
     * @var boolean
     */
	public $active = false;
	
	/**
     * Define the maximum number of places
	 * 0 -> infinity
     * 
     * @var integer
     */
	public $max_places = 0;
	
	/**
     * 
     * 
     * @var integer
     */
	public $children_price = 0;
	
	/**
     * 
     * 
     * @var integer
     */
	public $adults_price = 0;
	
	public function initialize(){
		$this->image = array();
		$this->sponsors = array();
	}
	
	public function getId() {
		return $this->_id;
	}

	public function getTitle() {
		return $this->title;
	}

	public function getContent() {
		return $this->content;
	}

	public function getEventDate() {
		return $this->event_date;
	}

	public function getImage() {
		return $this->image;
	}
	
	public function getSponsors(){
		return $this->sponsors;
	}

	public function getResume() {
		return $this->resume;
	}

	public function getActiveRegistrations() {
		return $this->active_registrations;
	}

	public function getActive() {
		return $this->active;
	}
	
	public function getMaxPlaces(){
		return $this->max_places;
	}

	public function setTitle($title) {
		$this->title = $title;
		return $this;
	}

	public function setContent($content) {
		$this->content = $content;
		return $this;
	}

	public function setEventDate($event_date) {
		$this->event_date = $event_date;
		return $this;
	}

	public function setImage($image) {
		array_push($this->image, $image);
		return $this;
	}
	
	public function setSponsors($sponsor){
		array_push($this->sponsors, $sponsor);
		return $this;
	}

	public function setResume($resume) {
		$this->resume = $resume;
		return $this;
	}

	public function setActiveRegistrations($active_registrations) {
		$this->active_registrations = $active_registrations;
		return $this;
	}

	public function setActive($active) {
		$this->active = $active;
		return $this;
	}

	public function setMaxPlaces($max_places){
		$this->max_places = $max_places;
		return $this;
	}
	
	public function getChildrenPrice() {
		return $this->children_price;
	}

	public function getAdultsPrice() {
		return $this->adults_price;
	}

	public function setChildrenPrice($children_price) {
		$this->children_price = $children_price;
		return $this;
	}

	public function setAdultsPrice($adults_price) {
		$this->adults_price = $adults_price;
		return $this;
	}

	public function getSpecialContent() {
		return $this->special_content;
	}

	public function setSpecialContent($special_content) {
		$this->special_content = $special_content;
		return $this;
	}

	public function getBookingStartDate() {
		return $this->booking_start_date;
	}

	public function setBookingStartDate(\MongoDate $booking_start_date) {
		$this->booking_start_date = $booking_start_date;
		return $this;
	}


}