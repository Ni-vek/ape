<?php
namespace Ecole\Models;

/**
 * Permissions
 * Stores the permissions by profile
 */
class Permissions extends BaseModel
{

    /**
     *
     * @var string
     */
    public $_id;

    /**
     *
     * @var \MongoId
     */
    public $profile_id;

    /**
     *
     * @var string
     */
    public $resource;

    /**
     *
     * @var string
     */
    public $action;

    public function getId()
    {
        return $this->_id;
    }

    public function getProfileId()
    {
        return $this->profile_id;
    }

    public function getResource()
    {
        return $this->resource;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function setId($id)
    {
        $this->_id = $id;
        return $this;
    }

    public function setProfileId(\MongoId $profile_id)
    {
        $this->profile_id = $profile_id;
        return $this;
    }

    public function setResource($resource)
    {
        $this->resource = $resource;
        return $this;
    }

    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }


}