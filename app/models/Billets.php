<?php

namespace Ecole\Models;

class Billets extends BaseModel {

	/**
     * The Billet object ID
     * 
     * @var \MongoID
     */
	public $_id;
	
	/**
     *
     * 
     * @var string
     */
	public $titre;
	
	/**
     *
     * 
     * @var string
     */
	public $contenu;
	
	/**
     *
     * 
     * @var \MongoDate
     */
	public $date_creation;
	
	/**
     *
     * 
     * @var string
     */
	public $image;
	
	/**
     *
     * 
     * @var string
     */
	public $resume;
	
	/**
     *
     * 
     * @var string
     */
	public $tag;
	
	/**
     *
     * 
     * @var \Ecole\Models\Commentaires
     */
	protected $commentaires_object = null;
	
	/**
     *
     * 
     * @var boolean
     */
	public $active = false;
	
	/**
     *
     * 
     * @var \MongoID
     */
	public $event_linked = null;
	
	/**
     *
     * 
     * @var boolean
     */
	public $commentaires_actifs = false;
	
	public function getId() {
		return $this->_id;
	}

	public function getTitre() {
		return $this->titre;
	}

	public function getContenu() {
		return $this->contenu;
	}

	public function getDateCreation() {
		return $this->date_creation;
	}

	public function getImage() {
		return $this->image;
	}

	public function getResume() {
		return $this->resume;
	}

	public function getTag() {
		return $this->tag;
	}

	public function getCommentaires() {
		if($this->commentaires_object === null){
			$this->commentaires_object = Commentaires::find(array(array(
				'billet' => $this->getId()
			)));
		}
		return $this->commentaires_object;
	}

	public function getActive() {
		return $this->active;
	}

	public function getEventLinked() {
		return $this->event_linked;
	}

	public function getCommentairesActifs() {
		return $this->commentaires_actifs;
	}

	public function setTitre($titre) {
		$this->titre = $titre;
		return $this;
	}

	public function setContenu($contenu) {
		$this->contenu = $contenu;
		return $this;
	}

	public function setDateCreation(\MongoDate $date_creation) {
		$this->date_creation = $date_creation;
		return $this;
	}

	public function setImage($image) {
		$this->image = $image;
		return $this;
	}

	public function setResume($resume) {
		$this->resume = $resume;
		return $this;
	}

	public function setTag($tag) {
		$this->tag = $tag;
		return $this;
	}

	public function setActive($active) {
		$this->active = $active;
		return $this;
	}

	public function setEventLinked(\MongoID $event_linked) {
		$this->event_linked = $event_linked;
		return $this;
	}

	public function setCommentairesActifs($commentaires_actifs) {
		$this->commentaires_actifs = $commentaires_actifs;
		return $this;
	}

	
	public function getCommentsLength() {
		if ($this->commentaires_object === null) {
			$this->commentaires_object = Commentaires::find(array(array(
				'billet' => $this->_id
			)));
		}
		return count($this->commentaires_object);
	}

}
