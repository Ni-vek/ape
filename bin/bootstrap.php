<?php

    error_reporting(E_ALL);
    /**
     * Define some useful constants
     */
    define('BASE_DIR', dirname(__DIR__));
    define('APP_DIR', BASE_DIR . '/app');
    define('APP_ENV', 'dev');

    /**
     * Define default timezone
     */
    date_default_timezone_set('GMT');

    /**
     * Read the configuration
     */
    $config = include APP_DIR . '/config/config_' . APP_ENV . '.php';

    /**
     * Read auto-loader
     */
    include APP_DIR . '/config/loader.php';

    /**
     * Read services
     */
    include APP_DIR . '/config/services.php';
