<?php

set_time_limit(0);
echo '==========================================' . PHP_EOL;
echo '           Creating cities' . PHP_EOL;
echo '==========================================' . PHP_EOL;
echo 'Opening CSV file...';
$file = fopen(APP_DIR . '/../bin/fixtures/load_cities/cities_france.csv', 'a+');
echo 'Done!' . PHP_EOL;

$fields = array(
    '_id',
    'departement',
    'slug',
    'name',
    'real_name',
    'soundex',
    'metaphone',
    'zip_code',
    'city_departement_code',
    'city_code',
    'city_part',
    'city_canton',
    'amdi',
    'population_2010',
    'population_1999',
    'population_2012',
    'densite_2010',
    'surface',
    'longitude_deg',
    'latitude_deg',
    'longitude_grd',
    'latitude_grd',
    'longitude_dms',
    'latitude_dms',
    'zmin',
    'zmax',
    'population_2010_order',
    'densite_2010_order',
    'surface_order'
);
$j = 0;
while ($tab = fgetcsv($file, 0, "\t")) {
    if ($j > 29949) {
        $city = new \Nannyster\Models\Cities();
        echo '[' . $j . '/36568] Creating city...';
        for ($i = 0; $i < count($tab); $i ++) {
            if ($i > 0) {
                $setter = 'set' . \Nannyster\Utils\CamelCase::to_camel_case($fields[$i], true);
                $city->$setter($tab[$i]);
            }
        }
        echo 'Saving...';
        $city->save();
        echo 'Done! Mem : ' . round((memory_get_usage() / 1024 / 1024), 3) . ' MB. ' . PHP_EOL;
    }
    $j++;
}