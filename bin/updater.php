<?php

require 'bootstrap.php';

$logger = $di->getShared('logger');

$accounts_to_update = \IGTrading\Models\Accounts::find(array(array(
                'deleted' => false)));
$logger->info(count($accounts_to_update) . ' to update!');
if($accounts_to_update){
    foreach ($accounts_to_update as $account_to_update) {
        $logger->info('Starting updating account ' . $account_to_update->getName());
        try {
            $connector = new \IGTrading\Connectors\CommonConnector($account_to_update);
    //            if($account_to_update->getLastUpdate() === null){
    //                $connector->initialize_account();
    //            }
            $account = $connector->update_account();

            $account->setStatus('On')
                    ->save();
            $logger->info('Account ' . $account->getId() . ' - ' . $account->getName() . ' updated successfully!');
        }
        catch (\Exception $e) {
            $logger->warning('Account ' . $account_to_update->getId() . ' - ' . $account_to_update->getName() . ' can\'t be updated! ' . $e->getMessage());
        }

    }
}
else{
    $logger->warning('No accounts to update');
}
$logger->info('Update finished');
$logger->info('-----------------------------------------------------------------------------------------------');